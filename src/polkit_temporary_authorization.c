/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_temporary_authorization.h"
#include "typeobjects/temporary_authorization.h"

void _pypolkit_PolkitTemporaryAuthorization_dealloc(_pypolkit_PolkitTemporaryAuthorization *self) 
{
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitTemporaryAuthorization_traverse(_pypolkit_PolkitTemporaryAuthorization *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitTemporaryAuthorization_clear(_pypolkit_PolkitTemporaryAuthorization *self) 
{
    return 0;
}

int _pypolkit_PolkitTemporaryAuthorization_init(_pypolkit_PolkitTemporaryAuthorization *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject * pypolkit_polkit_temporary_authorization_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitTemporaryAuthorization *authorization = NULL;
    PyObject *py_subject = NULL;

    const gchar *id;
    const gchar *action_id;
    PolkitSubject *subject;
    guint64 time_obtained;
    guint64 time_expires;

    if(!PyArg_ParseTuple(args, "ssO!ll", &id, &action_id, 
                         &_pypolkit_PolkitSubject_Type_obj, &py_subject, 
                         &time_obtained, &time_expires)){
        return NULL;
    }

    authorization = polkit_temporary_authorization_new(id, action_id, subject, time_obtained, time_expires);
    if(authorization){
        ret = PolkitTemporaryAuthorization2_pypolkit_PolkitTemporaryAuthorization(authorization);
    }
    
    g_object_unref(authorization);

    return ret;
}

PyObject * pypolkit_polkit_temporary_authorization_get_id(PyObject *s, PyObject *args)
{
    const gchar *id;

    PolkitTemporaryAuthorization *temporaryauthorization = NULL;
    temporaryauthorization = _pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(s);
    if(temporaryauthorization == NULL){
        return NULL;
    }

    id = polkit_temporary_authorization_get_id(temporaryauthorization);

    g_object_unref(temporaryauthorization);

    return Py_BuildValue("s", id);
}

PyObject * pypolkit_polkit_temporary_authorization_get_action_id(PyObject *s, PyObject *args)
{
    const gchar *action_id;

    PolkitTemporaryAuthorization *temporaryauthorization = NULL;
    temporaryauthorization = _pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(s);
    if(temporaryauthorization == NULL){
        return NULL;
    }

    action_id = polkit_temporary_authorization_get_id(temporaryauthorization);

    g_object_unref(temporaryauthorization);

    return Py_BuildValue("s", action_id);
}

PyObject * pypolkit_polkit_temporary_authorization_get_subject(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitSubject *subject = NULL;
    PyObject *py_subject = NULL;

    PolkitTemporaryAuthorization *temporaryauthorization = NULL;
    temporaryauthorization = _pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(s);
    if(temporaryauthorization == NULL){
        return NULL;
    }

    subject = polkit_temporary_authorization_get_subject(temporaryauthorization);
    if(subject){
        ret = PolkitSubject2_pypolkit_PolkitSubject(subject);
    }

    g_object_unref(subject);
    g_object_unref(temporaryauthorization);

    return ret;
}

PyObject * pypolkit_polkit_temporary_authorization_get_time_obtained(PyObject *s, PyObject *args)
{
    guint64 time;

    PolkitTemporaryAuthorization *temporaryauthorization = NULL;
    temporaryauthorization = _pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(s);
    if(temporaryauthorization == NULL){
        return NULL;
    }

    time = polkit_temporary_authorization_get_time_obtained(temporaryauthorization);

    g_object_unref(temporaryauthorization);

    return Py_BuildValue("l", time);
}

PyObject * pypolkit_polkit_temporary_authorization_get_time_expires(PyObject *s, PyObject *args)
{
    guint64 time;

    PolkitTemporaryAuthorization *temporaryauthorization = NULL;
    temporaryauthorization = _pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(s);
    if(temporaryauthorization == NULL){
        return NULL;
    }

    time = polkit_temporary_authorization_get_time_expires(temporaryauthorization);
    
    g_object_unref(temporaryauthorization);

    return Py_BuildValue("l", time);
}
