/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_subject.h"
#include "typeobjects/subject.h"

void _pypolkit_PolkitSubject_dealloc(_pypolkit_PolkitSubject *self) {
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitSubject_traverse(_pypolkit_PolkitSubject *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitSubject_clear(_pypolkit_PolkitSubject *self) {

    return 0;
}

int _pypolkit_PolkitSubject_init(_pypolkit_PolkitSubject*self, PyObject *args, PyObject *kwds) {

    return 0;
}

PyObject * pypolkit_polkit_subject_hash(PyObject *s, PyObject *args)
{
    guint hash;

    PolkitSubject *polkitsubject = NULL;
    polkitsubject = _pypolkit_PolkitSubject2PolkitSubject(s);
    if(polkitsubject == NULL){
        return NULL;
    }

    hash = polkit_subject_hash(polkitsubject);

    g_object_unref(polkitsubject);

    return Py_BuildValue("i", hash);
}

PyObject * pypolkit_polkit_subject_equal(PyObject *s, PyObject *args)
{
    gboolean equal = FALSE;
    PolkitSubject *b = NULL, *polkitsubject = NULL;
    PyObject *py_subject = NULL;

    polkitsubject = _pypolkit_PolkitSubject2PolkitSubject(s);
    if(polkitsubject == NULL){
        return NULL;
    }
    if(!PyArg_ParseTuple("O!", &_pypolkit_PolkitSubject_Type_obj, &py_subject)){
        return NULL;
    }

    b = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(b == NULL){
        return NULL;
    }

    equal = polkit_subject_equal(polkitsubject, b);

    g_object_unref(polkitsubject);
    g_object_unref(b);

    if(equal){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_subject_exists(PyObject *s, PyObject *args)
{
    gboolean exists = FALSE;

    PolkitSubject *polkitsubject = NULL;
    polkitsubject = _pypolkit_PolkitSubject2PolkitSubject(s);
    if(polkitsubject == NULL){
        return NULL;
    }

    exists = polkit_subject_exists_sync(polkitsubject, NULL, NULL);

    g_object_unref(polkitsubject);

    if(exists){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_subject_to_string(PyObject *s, PyObject *args)
{
    gchar *string = NULL;
    
    PolkitSubject *polkitsubject = NULL;
    polkitsubject = _pypolkit_PolkitSubject2PolkitSubject(s);
    if(polkitsubject == NULL){
        return NULL;
    }

    string = polkit_subject_to_string(polkitsubject);

    g_object_unref(polkitsubject);
    
    return Py_BuildValue("s", string);
}

PyObject * pypolkit_polkit_subject_from_string(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    gchar *string = NULL;
    PolkitSubject *subject = NULL;
    if(!PyArg_ParseTuple("s", string)){
        return NULL;
    }

    subject = polkit_subject_from_string(string, NULL);
    if(subject){
        ret = PolkitSubject2_pypolkit_PolkitSubject(subject);
    }

    g_object_unref(subject);

    return ret;
}
