/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_identity.h"
#include "typeobjects/identity.h"

void _pypolkit_PolkitIdentity_dealloc(_pypolkit_PolkitIdentity *self) {
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitIdentity_traverse(_pypolkit_PolkitIdentity *self, visitproc visit, void *arg)
{
    return 0;
}


int _pypolkit_PolkitIdentity_clear(_pypolkit_PolkitIdentity *self) {

    return 0;
}

int _pypolkit_PolkitIdentity_init(_pypolkit_PolkitIdentity*self, PyObject *args, PyObject *kwds) {

    return 0;
}

PyObject * pypolkit_polkit_identity_new(PyObject *s, PyObject *args)
{
    /* guint hash = NULL; */
    /* PolkitIdentity *polkitidentity = NULL; */
    /* polkitidentity = _pypolkit_PolkitIdentity2PolkitIdentity(s); */
    /* if(polkitidentity == NULL){ */
    /*     return NULL; */
    /* } */

    /* hash = polkit_identity_hash(polkitidentity); */

    /* g_object_unref(polkitidentity); */

    /* return Py_BuildValue("i", hash); */
    ;
}

PyObject * pypolkit_polkit_identity_hash(PyObject *s, PyObject *args)
{
    guint hash = NULL;
    PolkitIdentity *polkitidentity = NULL;
    polkitidentity = _pypolkit_PolkitIdentity2PolkitIdentity(s);
    if(polkitidentity == NULL){
        return NULL;
    }

    hash = polkit_identity_hash(polkitidentity);

    g_object_unref(polkitidentity);

    return Py_BuildValue("i", hash);
}

PyObject * pypolkit_polkit_identity_equal(PyObject *s, PyObject *args)
{
    gboolean equal = FALSE;
    PolkitIdentity *polkitidentity = NULL, *b = NULL;
    PyObject *py_identity = NULL;

    polkitidentity = _pypolkit_PolkitIdentity2PolkitIdentity(s);
    if(polkitidentity == NULL){
        return NULL;
    }
    
    if(!PyArg_ParseTuple("O!", &_pypolkit_PolkitIdentity_Type_obj, &py_identity)){
        return NULL;
    }

    b = _pypolkit_PolkitIdentity2PolkitIdentity(py_identity);
    if(b == NULL){
        return NULL;
    }

    equal = polkit_identity_equal(polkitidentity, b);

    g_object_unref(polkitidentity);
    g_object_unref(b);

    if(equal){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_identity_to_string(PyObject *s, PyObject *args)
{
    gchar *string;

    PolkitIdentity *polkitidentity = NULL;
    polkitidentity = _pypolkit_PolkitIdentity2PolkitIdentity(s);
    if(polkitidentity == NULL){
        return NULL;
    }

    string = polkit_identity_to_string(polkitidentity);

    g_object_unref(polkitidentity);

    return Py_BuildValue("s", string);
}

PyObject * pypolkit_polkit_identity_from_string(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;

    gchar *string;
    PolkitIdentity *identity = NULL;

    if(!PyArg_ParseTuple("s", string)){
        return NULL;
    }

    identity = polkit_identity_from_string(string, NULL);
    if(identity){
        ret = PolkitIdentity2_pypolkit_PolkitIdentity(identity);
    }

    g_object_unref(identity);

    return ret;
}
