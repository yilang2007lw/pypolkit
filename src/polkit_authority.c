/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>
#include <polkit/polkit.h>
#include "convert.h"
#include "polkit_authority.h"
#include "typeobjects/authority.h"

void _pypolkit_PolkitAuthority_dealloc(_pypolkit_PolkitAuthority *self) 
{
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitAuthority_traverse(_pypolkit_PolkitAuthority *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitAuthority_clear(_pypolkit_PolkitAuthority *self) 
{

    return 0;
}

int _pypolkit_PolkitAuthority_init(_pypolkit_PolkitAuthority *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject *_pypolkit_PolkitAuthority_get(_pypolkit_PolkitAuthority *self, void *closure) {
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitAuthority()");
        return NULL;
    }

    if (!strcmp(member, "backend_name")) {
        return Py_BuildValue("s", self->backend_name);
    } else if (!strcmp(member, "backend_version")) {
        return Py_BuildValue("s", self->backend_version);
    } else if (!strcmp(member, "owner")) {
        return Py_BuildValue("s", self->owner);
    } else if (!strcmp(member, "backend_features")) {
        return Py_BuildValue("i", self->backend_features);
    }
}

int _pypolkit_PolkitAuthority_set(_pypolkit_PolkitAuthority *self, PyObject *value, void *closure) 
{
    printf("all readonly, can't set\n");
    return 0;
}

PyObject * pypolkit_polkit_authority_new(PyObject *s, PyObject *args)
{
    _pypolkit_PolkitAuthority *ret = NULL;
    PolkitAuthority *authority = NULL;

    authority = polkit_authority_get_sync(NULL, NULL);

    if(authority){
         ret = PolkitAuthority2_pypolkit_PolkitAuthority(authority);
    }

    g_object_unref(authority);

    return (PyObject *)ret;
}

PyObject * pypolkit_polkit_authority_check_authorization(PyObject *s, PyObject *args)
{
    _pypolkit_PolkitAuthorizationResult *ret = NULL;
    PolkitAuthorizationResult *result = NULL;

    PolkitAuthority *polkitauthority = NULL;
    PolkitSubject *subject = NULL;
    const gchar *action_id = NULL;
    PolkitDetails *details = NULL;
    int flags = 0;

    PyObject *py_subject = NULL, *py_details = NULL;

    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    if (!PyArg_ParseTuple(args, "O!sO!i", &_pypolkit_PolkitSubject_Type_obj, &subject, 
                          &action_id,
                          &_pypolkit_PolkitDetails_Type_obj, &details,
                          &flags)) {
        return NULL;
    }
    
    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }

    /* need action_id be checked??? */

    details = _pypolkit_PolkitDetails2PolkitDetails(py_details);
    if(details == NULL){
        return NULL;
    }

    result = polkit_authority_check_authorization_sync(polkitauthority, subject, action_id, details, flags, NULL, NULL);

    g_object_unref(polkitauthority);
    g_object_unref(subject);
    g_object_unref(details);

    if(result){
        ret =  PolkitAuthorizationResult2_pypolkit_PolkitAuthorizationResult(result);
    }else{
        return NULL;
    }    

    g_object_unref(result);

    return ret;
}

PyObject * pypolkit_polkit_authority_enumerate_actions(PyObject *s, PyObject *args)
{
    PyObject *ret_list = PyList_New(0);
    GList *actions, *gitem;
    
    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    actions = polkit_authority_enumerate_actions_sync(polkitauthority, NULL, NULL);
    if(actions){
        for(gitem = actions; gitem; gitem = g_list_next(gitem)){
            /* test gpointer for type of PolkitAction ???*/
            gpointer data = gitem->data;
            PyList_Append(ret_list, data);
            g_object_unref(data);
        }
    }else{
        return NULL;
    }

    g_list_free(actions);
    g_list_free(gitem);

    return ret_list;
}

PyObject * pypolkit_polkit_authority_register_authentication_agent(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    gboolean result = FALSE;

    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }
    
    PyObject *py_subject = NULL;
    PolkitSubject *subject = NULL;
    const gchar *locale = NULL;
    const gchar *object_path = NULL;
    
    if (!PyArg_ParseTuple(args, "O!ss",
                          &_pypolkit_PolkitSubject_Type_obj, &py_subject, &locale, &object_path)) {
        return NULL;
    }
    
    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }
    
    /* need check for local and object_path ???? */

    result = polkit_authority_register_authentication_agent_sync(polkitauthority, subject,
                                                                 locale, object_path, NULL, NULL);

    g_object_unref(polkitauthority);
    g_object_unref(subject);

    if(result){
        ret = Py_True;
    }else{
        ret =  Py_False;
    }

    return ret;
}

PyObject * pypolkit_polkit_authority_register_authentication_agent_with_options(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    PolkitSubject *subject;
    const gchar *locale;
    const gchar *object_path;
    GVariant *options;

    PyObject *py_subject;

    /* how to parse GVariant ???? */

    if (!PyArg_ParseTuple(args, "O!ssO!",
                          &_pypolkit_PolkitSubject_Type_obj, &subject, 
                          &locale, &object_path, &options)) {
        return NULL;
    }

    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }
    /* fix this??? */
    /* result = polkit_authority_register_authentication_agent_with_options_sync(polkitauthority, subject, */
    /*                                                              locale, object_path, options, NULL, NULL); */
    result = TRUE;
    g_object_unref(polkitauthority);
    g_object_unref(subject);
    g_variant_unref(options);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authority_unregister_authentication_agent(PyObject *s, PyObject *args)
{
    gboolean result;

    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    PolkitSubject *subject;
    const gchar *object_path;
    PyObject *py_subject;

    if (!PyArg_ParseTuple(args, "O!s",
                          &_pypolkit_PolkitSubject_Type_obj, &py_subject, &object_path)) {
        return NULL;
    }

    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }
    
    if(object_path == NULL){
        return NULL;
    }

    result = polkit_authority_unregister_authentication_agent_sync(polkitauthority, subject,
                                                                   object_path, NULL, NULL);

    g_object_unref(polkitauthority);
    g_object_unref(subject);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authority_authentication_agent_response(PyObject *s, PyObject *args)
{
    gboolean result;

    PolkitAuthority *polkitauthority;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    const gchar *cookie;
    PolkitIdentity *identity = NULL;
    PyObject *py_identity = NULL;

    if (!PyArg_ParseTuple(args, "sO!",
                          &cookie, &_pypolkit_PolkitIdentity_Type_obj, &identity)) {
        return NULL;
    }

    if(cookie == NULL){
        return NULL;
    }

    identity = _pypolkit_PolkitIdentity2PolkitIdentity(py_identity);
    if(identity == NULL){
        return NULL;
    }

    result = polkit_authority_authentication_agent_response_sync(polkitauthority, cookie,
                                                                 identity, NULL, NULL);

    g_object_unref(polkitauthority);
    g_object_unref(identity);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authority_enumerate_temporary_authorizations(PyObject *s, PyObject *args)
{
    PyObject *ret_list = PyList_New(0);
    GList *temporary_authorizations, *gitem;

    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    PolkitSubject *subject = NULL;
    PyObject *py_subject = NULL;

    if (!PyArg_ParseTuple(args, "O!", &_pypolkit_PolkitSubject_Type_obj, &py_subject)) {
        return NULL;
    }

    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }

    temporary_authorizations = polkit_authority_enumerate_temporary_authorizations_sync(polkitauthority,
                                                                                        subject, NULL, NULL);
    if(temporary_authorizations){
        for(gitem = temporary_authorizations; gitem; gitem = g_list_next(gitem)){
            PolkitTemporaryAuthorization *data = gitem->data;
            PyList_Append(ret_list, data);
            g_object_unref(data);
        }
    }else{
        ;
    }

    g_list_free(temporary_authorizations);
    g_list_free(gitem);

    g_object_unref(polkitauthority);
    g_object_unref(subject);

    return ret_list;
}

PyObject * pypolkit_polkit_authority_revoke_temporary_authorizations(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;
    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    PolkitSubject *subject;
    PyObject *py_subject;

    if (!PyArg_ParseTuple(args, "O!", &_pypolkit_PolkitSubject_Type_obj, &py_subject)) {
        return NULL;
    }

    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }

    result = polkit_authority_revoke_temporary_authorizations_sync(polkitauthority, subject, NULL, NULL);

    g_object_unref(polkitauthority);
    g_object_unref(subject);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authority_revoke_temporary_authorizations_by_id(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthority *polkitauthority = NULL;
    polkitauthority = _pypolkit_PolkitAuthority2PolkitAuthority(s);
    if(polkitauthority == NULL){
        return NULL;
    }

    const gchar *id;
    if (!PyArg_ParseTuple(args, "s", &id)) {
        return NULL;
    }

    if(id == NULL){
        return NULL;
    }

    result = polkit_authority_revoke_temporary_authorization_by_id_sync(polkitauthority, id, NULL, NULL);

    g_object_unref(polkitauthority);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}
