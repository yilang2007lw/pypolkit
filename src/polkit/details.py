#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 ~ 2013 Deepin, Inc.
#               2012 ~ 2013 Long Wei
#
# Author:     Long Wei <yilang2007lw@gmail.com>
# Maintainer: Long Wei <yilang2007lw@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pypolkit

class PolkitDetails(object):

    def __init__(self, details = None):
        if details:
            self.__details = details
        else:
            self.__details = pypolkit.details_new()

    def lookup(self, key):
        return self.__details.lookup(key)

    def insert(self, key, value):
        return self.__details.insert(key, value)

    def get_keys(self):
        return self.__details.get_keys()


if __name__ == "__main__":
    details = PolkitDetails()
    # print details.get_keys()
