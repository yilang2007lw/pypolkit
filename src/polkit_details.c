/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_details.h"
#include "typeobjects/details.h"

void _pypolkit_PolkitDetails_dealloc(_pypolkit_PolkitDetails *self) {
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitDetails_traverse(_pypolkit_PolkitDetails *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitDetails_clear(_pypolkit_PolkitDetails *self) {

    return 0;
}

int _pypolkit_PolkitDetails_init(_pypolkit_PolkitDetails*self, PyObject *args, PyObject *kwds) {

    return 0;
}

PyObject * pypolkit_polkit_details_new(PyObject *s, PyObject *args)
{
    _pypolkit_PolkitDetails *ret = NULL;
    PolkitDetails *details = NULL;

    details = polkit_details_new();

    if(details){
        ret = PolkitDetails2_pypolkit_PolkitDetails(details);
    }
    
    g_object_unref(details);

    return (PyObject *)ret;
}

PyObject * pypolkit_polkit_details_lookup(PyObject *s, PyObject *args)
{
    const gchar *value;
    const gchar *key;

    PolkitDetails *polkitdetails = NULL;
    polkitdetails = _pypolkit_PolkitDetails2PolkitDetails(s);
    if(polkitdetails == NULL){
        return NULL;
    }

    if (!PyArg_ParseTuple(args, "s", &key)) {
        return NULL;
    }

    value = polkit_details_lookup(polkitdetails, key);

    g_object_unref(polkitdetails);

    return Py_BuildValue("s", value);
}

PyObject * pypolkit_polkit_details_insert(PyObject *s, PyObject *args)
{
    const gchar *value;
    const gchar *key;

    PolkitDetails *polkitdetails;
    polkitdetails = _pypolkit_PolkitDetails2PolkitDetails(s);
    if(polkitdetails == NULL){
        return NULL;
    }

    if (!PyArg_ParseTuple(args, "ss", &key, &value)) {
        return NULL;
    }

    polkit_details_insert(polkitdetails, key, value);

    g_object_unref(polkitdetails);

    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * pypolkit_polkit_details_get_keys(PyObject *s, PyObject *args)
{
    PyObject *ret;
    gchar **keys;
    PolkitDetails *polkitdetails;

    polkitdetails = _pypolkit_PolkitDetails2PolkitDetails(s);
    if(polkitdetails == NULL){
        return NULL;
    }

    keys = polkit_details_get_keys(polkitdetails);

    g_object_unref(polkitdetails);

    /* is this return correct??? */
    return Py_BuildValue("[s]", keys);
}
