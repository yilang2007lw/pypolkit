/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_permission.h"
#include "typeobjects/permission.h"

void _pypolkit_PolkitPermission_dealloc(_pypolkit_PolkitPermission *self) {
    PyObject_GC_UnTrack(self);
    Py_CLEAR(self->subject);
    self->subject = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitPermission_traverse(_pypolkit_PolkitPermission *self, visitproc visit, void *arg)
{
    Py_VISIT(self->subject);
    return 0;
}

int _pypolkit_PolkitPermission_clear(_pypolkit_PolkitPermission *self) 
{
    Py_CLEAR(self->subject);
    return 0;
}

int _pypolkit_PolkitPermission_init(_pypolkit_PolkitPermission *self, PyObject *args, PyObject *kwds) {
    gchar *action_id = NULL;
    PyObject *py_subject = NULL;
    gboolean allowed = 0;
    gboolean can_acquire = 0;
    gboolean can_release = 0;
    
    if(!PyArg_ParseTuple(args, "|sO!iii", &action_id, 
                         &_pypolkit_PolkitSubject_Type_obj, &py_subject,
                         &allowed, &can_acquire, &can_release)){
        return NULL;
    }
    self->action_id = action_id;
    self->subject = py_subject;
    self->allowed = allowed;
    self->can_acquire = can_acquire;
    self->can_release = can_release;

    return 0;
}

PyObject *_pypolkit_PolkitPermission_get(_pypolkit_PolkitPermission *self, void *closure) {
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitPermission");
        return NULL;
    }

    if (!strcmp(member, "action_id")) {
        return Py_BuildValue("s", self->action_id);
    } else if (!strcmp(member, "subject")) {
        return Py_BuildValue("O", self->subject);
    } else if (!strcmp(member, "allowed")) {
        if(self->allowed){
            return Py_True;
        }else{
            return Py_False;
        }

    } else if (!strcmp(member, "can_acquire")) {
        if(self->can_acquire){
            return Py_True;
        }else{
            return Py_False;
        }
    } else if (!strcmp(member, "can_release")) {
        if(self->can_release){
            return Py_True;
        }else{
            return Py_False;
        }
    }
}

int _pypolkit_PolkitPermission_set(_pypolkit_PolkitPermission *self, PyObject *value, void *closure) 
{
    return 0;
}

PyObject * pypolkit_polkit_permission_new(PyObject *s, PyObject *args)
{
    PyObject  *ret = NULL;
    PolkitPermission *permission = NULL;

    const gchar *action_id;
    PolkitSubject *subject = NULL;
    PyObject *py_subject = NULL;

    if(!PyArg_ParseTuple(args, "s|O!", &action_id,  &_pypolkit_PolkitSubject_Type_obj, &py_subject)){
        return NULL;
    }

    subject = _pypolkit_PolkitSubject2PolkitSubject(py_subject);
    if(subject == NULL){
        return NULL;
    }

    permission = polkit_permission_new_sync(action_id, subject, NULL, NULL);

    if(permission){
        ret = PolkitPermission2_pypolkit_PolkitPermission(permission);
    }
    
    g_object_unref(permission);
    g_object_unref(subject);

    return ret;
}

PyObject * pypolkit_polkit_permission_acquire(PyObject *s, PyObject *args)
{
    gboolean acquire = FALSE;

    PolkitPermission *polkitpermission = NULL;
    polkitpermission = _pypolkit_PolkitPermission2PolkitPermission(s);
    if(polkitpermission == NULL){
        return Py_False;
    }

    acquire = g_permission_acquire((GPermission *)polkitpermission, NULL, NULL);
    
    g_object_unref(polkitpermission);

    if(acquire){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_permission_release(PyObject *s, PyObject *args)
{
    gboolean release = FALSE;

    PolkitPermission *polkitpermission = NULL;
    polkitpermission = _pypolkit_PolkitPermission2PolkitPermission(s);
    if(polkitpermission == NULL){
        return Py_False;
    }

    release= g_permission_release((GPermission *)polkitpermission, NULL, NULL);

    g_object_unref(polkitpermission);
    
    if(release){
        return Py_True;
    }else{
        return Py_False;
    }
}
