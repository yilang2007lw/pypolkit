/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_unixsession.h"
#include "typeobjects/unixsession.h"

void _pypolkit_PolkitUnixSession_dealloc(_pypolkit_PolkitUnixSession *self) 
{
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitUnixSession_traverse(_pypolkit_PolkitUnixSession *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitUnixSession_clear(_pypolkit_PolkitUnixSession *self) 
{
    return 0;
}

int _pypolkit_PolkitUnixSession_init(_pypolkit_PolkitUnixSession *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject *_pypolkit_PolkitUnixSession_get(_pypolkit_PolkitUnixSession *self, void *closure) 
{
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixSession");
        return NULL;
    }

    if (!strcmp(member, "pid")) {
        return Py_BuildValue("i", self->pid);
    }else if (!strcmp(member, "session_id")) {
        return self->session_id;
    } 
}

int _pypolkit_PolkitUnixSession_set(_pypolkit_PolkitUnixSession *self, PyObject *value, void *closure) 
{
    char *member = (char *) closure;
    PyObject *prop = NULL;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnix");
        return NULL;
    }

    if (!strcmp(member, "pid")) {
        if(!PyArg_ParseTuple(value, "i", &prop)){
            return NULL;
        }
        if(prop == NULL){
            return NULL;
        }
        self->pid = prop;
    }else if (!strcmp(member, "session_id")) {
        if(!PyArg_ParseTuple(value, "s", &prop)){
            return NULL;
        }
        if(prop == NULL){
            return NULL;
        }
        self->session_id= prop;
    }
    return 0;
}

PyObject * pypolkit_polkit_unix_session_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitUnixSession *unixsession = NULL;
    const gchar *sessio_id;

    if(!PyArg_ParseTuple(args, "s", &sessio_id)){
        return NULL;
    }

    unixsession = (PolkitUnixSession *)polkit_unix_session_new(sessio_id);
    if(unixsession){
        ret = PolkitUnixSession2_pypolkit_PolkitUnixSession(unixsession);
    }

    g_object_unref(unixsession);

    return ret;
}
