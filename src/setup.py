#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 ~ 2013 Deepin, Inc.
#               2012 ~ 2013 Long Wei
#
# Author:     Long Wei <yilang2007lw@gmail.com>
# Maintainer: Long Wei <yilang2007lw@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, Extension
import commands

def pkg_config_cflags(pkgs):
    return map(lambda path: path[2::], commands.getoutput('pkg-config --cflags-only-I %s' % (' '.join(pkgs))).split())
    
polkit_mod = Extension('pypolkit',
                                 include_dirs = pkg_config_cflags(['glib-2.0', 'gio-2.0', 'polkit-gobject-1']),
                                 libraries = ['glib-2.0', 'gio-2.0', 'polkit-gobject-1'],
                                 sources = ['polkit_authority.c', 'polkit_action_description.c', 'polkit_authorization_result.c',
                                            'polkit_details.c', 'polkit_identity.c', 'polkit_permission.c', 'polkit_subject.c',
                                            'polkit_system_busname.c', 'polkit_temporary_authorization.c', 'polkit_unixgroup.c',
                                            'polkit_unixnetgroup.c', 'polkit_unixprocess.c', 'polkit_unixsession.c',
                                            'polkit_unixuser.c', 'convert.c', 'pypolkit_module.c']
                                 )


setup(name = 'pypolkit',
      version = '0.1',
      ext_modules = [polkit_mod],
      description = "python bindings for PolicyKit",
      long_description = '''python binding for PolicyKit''',
      author = 'Long Wei',
      author_email = 'yilang2007lw@gmail.com',
      license = 'GPL-3',
      url = "www.linuxdeepin.com",
      download_url = "www.linuxdeepin.com",
      platforms = ['Linux']

      )
