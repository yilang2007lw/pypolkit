/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_authorization_result.h"
#include "typeobjects/authorization_result.h"

void _pypolkit_PolkitAuthorizationResult_dealloc(_pypolkit_PolkitAuthorizationResult *self) 
{
    PyObject_GC_UnTrack(self);
    Py_CLEAR(self->details);
    self->details = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitAuthorizationResult_clear(_pypolkit_PolkitAuthorizationResult *self) 
{
    Py_CLEAR(self->details);
    self->details = NULL;
    return 0;
}


int _pypolkit_PolkitAuthorizationResult_traverse(_pypolkit_PolkitAuthorizationResult *self, visitproc visit, void *arg)
{
    Py_VISIT(self->details);
    return 0;
}

int _pypolkit_PolkitAuthorizationResult_init(_pypolkit_PolkitAuthorizationResult *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject * pypolkit_polkit_authorization_result_new(PyObject *s, PyObject *args)
{
    _pypolkit_PolkitAuthorizationResult *ret = NULL;
    PolkitAuthorizationResult *result = NULL;

    gboolean is_authorized = 0;
    gboolean is_challenge = 0;
    PolkitDetails *details = NULL;
    PyObject *py_details = NULL;

    if(!PyArg_ParseTuple(args, "iiO!", &is_authorized, &is_challenge, &_pypolkit_PolkitDetails_Type_obj, &py_details)){
        return NULL;
    }

    details = _pypolkit_PolkitDetails2PolkitDetails(py_details);
    if(details == NULL){
        return NULL;
    }

    result = polkit_authorization_result_new(is_authorized, is_challenge, details);
    if(result){
        ret = PolkitAuthorizationResult2_pypolkit_PolkitAuthorizationResult(result);
    }

    g_object_unref(result);
    g_object_unref(details);

    return (PyObject *)ret;
}

PyObject * pypolkit_polkit_authorization_result_get_is_authorized(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }

    result = polkit_authorization_result_get_is_authorized(authorizationresult);

    g_object_unref(authorizationresult);
    
    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authorization_result_get_is_challenge(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }

    result = polkit_authorization_result_get_is_challenge(authorizationresult);

    g_object_unref(authorizationresult);
    
    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authorization_result_get_retains_authorization(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }

    result = polkit_authorization_result_get_retains_authorization(authorizationresult);

    g_object_unref(authorizationresult);
    
    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authorization_result_get_temporary_authorization_id(PyObject *s, PyObject *args)
{
    const gchar *temporary_id = NULL;
    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }
    
    temporary_id = polkit_authorization_result_get_temporary_authorization_id(authorizationresult);

    g_object_unref(authorizationresult);

    if(temporary_id == NULL){
        Py_INCREF(Py_None);
        return Py_None;
    }
    return Py_BuildValue("s", temporary_id);
}

PyObject * pypolkit_polkit_authorization_result_get_dismissed(PyObject *s, PyObject *args)
{
    gboolean result = FALSE;

    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }

    result = polkit_authorization_result_get_dismissed(authorizationresult);

    g_object_unref(authorizationresult);

    if(result){
        return Py_True;
    }else{
        return Py_False;
    }
}

PyObject * pypolkit_polkit_authorization_get_details(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitDetails *details = NULL;
    PolkitAuthorizationResult *authorizationresult = NULL;
    authorizationresult = _pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(s);
    if(authorizationresult == NULL){
        return NULL;
    }

    details = polkit_authorization_result_get_dismissed(authorizationresult);

    ret = PolkitDetails2_pypolkit_PolkitDetails(details);

    g_object_unref(authorizationresult);
    g_object_unref(details);
    
    return ret;
}
