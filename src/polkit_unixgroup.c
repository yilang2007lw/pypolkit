/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_unixgroup.h"
#include "typeobjects/unixgroup.h"

void _pypolkit_PolkitUnixGroup_dealloc(_pypolkit_PolkitUnixGroup *self) 
{
    PyObject_GC_UnTrack(self);
    self->gid = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitUnixGroup_traverse(_pypolkit_PolkitUnixGroup *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitUnixGroup_clear(_pypolkit_PolkitUnixGroup *self) 
{

    return 0;
}

int _pypolkit_PolkitUnixGroup_init(_pypolkit_PolkitUnixGroup *self, PyObject *args, PyObject *kwds) 
{

    return 0;
}

PyObject *_pypolkit_PolkitUnixGroup_get(_pypolkit_PolkitUnixGroup *self, void *closure) 
{
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixGroup");
        return NULL;
    }

    if (!strcmp(member, "gid")) {
        return Py_BuildValue("i", self->gid);
    } 
}

int _pypolkit_PolkitUnixGroup_set(_pypolkit_PolkitUnixGroup *self, PyObject *value, void *closure) 
{
    char *member = (char *) closure;
    gint gid = NULL;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixGroup");
        return NULL;
    }

    if(!PyArg_ParseTuple(value, "i", &gid)){
        return NULL;
    }

    if(gid == NULL){
        return NULL;
    }

    if (!strcmp(member, "gid")) {
        return NULL;
    } 
    
    self->gid = gid;

    return 0;
}

PyObject * pypolkit_polkit_unix_group_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitUnixGroup *group = NULL;
    gint gid;

    if(!PyArg_ParseTuple(args, "i", &gid)){
        return NULL;
    }

    group = polkit_unix_group_new(gid);
    
    if(group){
        ret = PolkitUnixGroup2_pypolkit_PolkitUnixGroup(group);
    }
    
    g_object_unref(group);

    return ret;
}
