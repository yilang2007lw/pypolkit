/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_action_description.h"
#include "typeobjects/action_description.h"

void _pypolkit_PolkitActionDescription_dealloc(_pypolkit_PolkitActionDescription *self) 
{
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitActionDescription_traverse(_pypolkit_PolkitActionDescription *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitActionDescription_clear(_pypolkit_PolkitActionDescription *self) 
{
    return 0;
}

int _pypolkit_PolkitActionDescription_init(_pypolkit_PolkitActionDescription *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject * pypolkit_polkit_action_description_new(PyObject *s, PyObject *args)
{
    /* currently not support annotation, need fix  */
    _pypolkit_PolkitActionDescription *ret = NULL;

    PolkitActionDescription *action_description = NULL;

    const gchar *action_id = NULL;
    const gchar *description = NULL;
    const gchar *message = NULL;
    const gchar *vendor_name = NULL;
    const gchar *vendor_url = NULL;
    int implicit_any = 0;
    int implicit_inactive = 0;
    int implicit_active = 0;

    if(!PyArg_ParseTuple(args, "s|ssssiii", &action_id, &description, &message, &vendor_name, &vendor_url,
                         &implicit_any, &implicit_inactive, &implicit_active)){
        return NULL;
    }

    action_description = polkit_action_description_new(action_id, description, message, vendor_name, vendor_url,
                                                       implicit_any, implicit_inactive, implicit_active, NULL);

    if(action_description){
        ret = PolkitActionDescription2_pypolkit_PolkitActionDescription(action_description);
    }

    g_object_unref(action_description);

    return (PyObject *)ret;
}

PyObject * pypolkit_polkit_action_description_get_action_id(PyObject *s, PyObject *args)
{
    const gchar *action_id;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    action_id = polkit_action_description_get_action_id(actiondescription);

    if(action_id != NULL){
        return Py_BuildValue("s", action_id);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_description(PyObject *s, PyObject *args)
{
    const gchar *description;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    description = polkit_action_description_get_description(actiondescription);

    if(description != NULL){
        return Py_BuildValue("s", description);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_message(PyObject *s, PyObject *args)
{
    const gchar *message;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    message = polkit_action_description_get_message(actiondescription);

    if(message != NULL){
        return Py_BuildValue("s", message);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_vendor_name(PyObject *s, PyObject *args)
{
    const gchar *vendor_name;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    vendor_name = polkit_action_description_get_vendor_name(actiondescription);

    if(vendor_name != NULL){
        return Py_BuildValue("s", vendor_name);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_vendor_url(PyObject *s, PyObject *args)
{
    const gchar *vendor_url;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    vendor_url = polkit_action_description_get_vendor_url(actiondescription);

    if(vendor_url != NULL){
        return Py_BuildValue("s", vendor_url);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_icon_name(PyObject *s, PyObject *args)
{
    const gchar *icon_name;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    icon_name = polkit_action_description_get_icon_name(actiondescription);

    if(icon_name != NULL){
        return Py_BuildValue("s", icon_name);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}


PyObject * pypolkit_polkit_action_description_get_implicit_any(PyObject *s, PyObject *args)
{
    /* enum PolkitImplictAuthorization auth; */
    int auth;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    auth = polkit_action_description_get_implicit_any(actiondescription);

    if(auth != NULL){
        return Py_BuildValue("i", auth);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_implicit_inactive(PyObject *s, PyObject *args)
{
    /* enum PolkitImplictAuthorization auth; */
    int auth;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    auth = polkit_action_description_get_implicit_inactive(actiondescription);

    if(auth != NULL){
        return Py_BuildValue("i", auth);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_implicit_active(PyObject *s, PyObject *args)
{
    /* enum PolkitImplictAuthorization auth; */
    int auth;
    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    auth = polkit_action_description_get_implicit_active(actiondescription);

    if(auth != NULL){
        return Py_BuildValue("i", auth);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyObject * pypolkit_polkit_action_description_get_annotation(PyObject *s, PyObject *args)
{
    const gchar *annotation;
    const gchar *key;

    PolkitActionDescription *actiondescription;
    actiondescription = _pypolkit_PolkitActionDescription2PolkitActionDescription(s);

    if(actiondescription == NULL){
        return NULL;
    }

    if(!PyArg_ParseTuple("s", &key)){
        return -1;
    }

    annotation = polkit_action_description_get_annotation(actiondescription, key);

    if(annotation != NULL){
        return Py_BuildValue("s", annotation);
    }else{
        Py_INCREF(Py_None);
        return Py_None;
    }
}
