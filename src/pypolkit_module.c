/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>
#include <polkit/polkit.h>

#include "convert.h"
#include "polkit_action_description.h"
#include "polkit_authority.h"
#include "polkit_authorization_result.h"
#include "polkit_details.h"
#include "polkit_identity.h"
#include "polkit_permission.h"
#include "polkit_subject.h"
#include "polkit_system_busname.h"
#include "polkit_temporary_authorization.h"
#include "polkit_unixgroup.h"
#include "polkit_unixnetgroup.h"
#include "polkit_unixprocess.h"
#include "polkit_unixsession.h"
#include "polkit_unixuser.h"

static PyMethodDef _pypolkit_module_methods[] = {

    /* {"action_description_new", (PyCFunction) pypolkit_polkit_action_description_new, METH_VARARGS, "action description new"}, */
    {"authority_new", (PyCFunction) pypolkit_polkit_authority_new, METH_VARARGS, "authority new"},
    {"authorization_result_new", (PyCFunction) pypolkit_polkit_authorization_result_new, METH_VARARGS, "doc"},
    {"details_new", (PyCFunction) pypolkit_polkit_details_new, METH_VARARGS, "details new"},
    {"permission_new", (PyCFunction) pypolkit_polkit_permission_new, METH_VARARGS, "permission new"},
    {"system_busname_new", (PyCFunction) pypolkit_polkit_system_bus_name_new, METH_VARARGS, "system busname new"},
    {"unix_group_new", (PyCFunction) pypolkit_polkit_unix_group_new, METH_VARARGS, "unix group new"},
    {"unix_netgroup_new", (PyCFunction) pypolkit_polkit_unix_netgroup_new, METH_VARARGS, "unix netgroup new"},
    {"unix_process_new", (PyCFunction) pypolkit_polkit_unix_process_new, METH_VARARGS, "unix process new"},
    {"unix_session_new", (PyCFunction) pypolkit_polkit_unix_session_new, METH_VARARGS, "unix session new"},
    {"unix_user_new", (PyCFunction) pypolkit_polkit_unix_user_new, METH_VARARGS, "unix user new"},
    {"temporary_authorization_new", (PyCFunction) pypolkit_polkit_temporary_authorization_new, METH_VARARGS, "doc"},

    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initpypolkit(void) {
    PyObject *m = NULL;

    m = Py_InitModule3("pypolkit", _pypolkit_module_methods, "pypolkit module doc");

    if(!m){
        return ;
    }

    g_type_init();

    /* Add ActionDescription */
    if (PyType_Ready(&_pypolkit_PolkitActionDescription_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitActionDescription_Type_obj);
    PyModule_AddObject(m, "ActionDescription",
                       (PyObject *)&_pypolkit_PolkitActionDescription_Type_obj);

    /* Add Authority */
    if (PyType_Ready(&_pypolkit_PolkitAuthority_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitAuthority_Type_obj);
    PyModule_AddObject(m, "Authority",
                       (PyObject *)&_pypolkit_PolkitAuthority_Type_obj);
    
    /* Add AuthorizationResult */
    if (PyType_Ready(&_pypolkit_PolkitAuthorizationResult_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitAuthorizationResult_Type_obj);
    PyModule_AddObject(m, "AuthorizationResult",
                       (PyObject *)&_pypolkit_PolkitAuthorizationResult_Type_obj);
    

    /* Add Details */
    if (PyType_Ready(&_pypolkit_PolkitDetails_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitDetails_Type_obj);
    PyModule_AddObject(m, "Details",
                       (PyObject *)&_pypolkit_PolkitDetails_Type_obj);
    
    /* Add Identity */
    if (PyType_Ready(&_pypolkit_PolkitIdentity_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitIdentity_Type_obj);
    PyModule_AddObject(m, "Identity",
                       (PyObject *)&_pypolkit_PolkitIdentity_Type_obj);
    
    /* Add Permission */
    if (PyType_Ready(&_pypolkit_PolkitPermission_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitPermission_Type_obj);
    PyModule_AddObject(m, "Permission",
                       (PyObject *)&_pypolkit_PolkitPermission_Type_obj);
    
    /* Add Subject */
    if (PyType_Ready(&_pypolkit_PolkitSubject_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitSubject_Type_obj);
    PyModule_AddObject(m, "Subject",
                       (PyObject *)&_pypolkit_PolkitSubject_Type_obj);
    
    /* Add SystemBusName */
    if (PyType_Ready(&_pypolkit_PolkitSystemBusName_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitSystemBusName_Type_obj);
    PyModule_AddObject(m, "SystemBusName",
                       (PyObject *)&_pypolkit_PolkitSystemBusName_Type_obj);
    
    /* Add TemporaryAuthorization */
    if (PyType_Ready(&_pypolkit_PolkitTemporaryAuthorization_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitTemporaryAuthorization_Type_obj);
    PyModule_AddObject(m, "TemporaryAuthorization",
                       (PyObject *)&_pypolkit_PolkitTemporaryAuthorization_Type_obj);
    
    /* Add UnixGroup */
    if (PyType_Ready(&_pypolkit_PolkitUnixGroup_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitUnixGroup_Type_obj);
    PyModule_AddObject(m, "UnixGroup",
                       (PyObject *)&_pypolkit_PolkitUnixGroup_Type_obj);
    
    /* Add UnixNetGroup */
    if (PyType_Ready(&_pypolkit_PolkitUnixNetGroup_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitUnixNetGroup_Type_obj);
    PyModule_AddObject(m, "UnixNetGroup",
                       (PyObject *)&_pypolkit_PolkitUnixNetGroup_Type_obj);
    
    /* Add UnixProcess */
    if (PyType_Ready(&_pypolkit_PolkitUnixProcess_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitUnixProcess_Type_obj);
    PyModule_AddObject(m, "UnixProcess",
                       (PyObject *)&_pypolkit_PolkitUnixProcess_Type_obj);
    
    /* Add UnixSession */
    if (PyType_Ready(&_pypolkit_PolkitUnixSession_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitUnixSession_Type_obj);
    PyModule_AddObject(m, "UnixSession",
                       (PyObject *)&_pypolkit_PolkitUnixSession_Type_obj);
    
    /* Add UnixUser */
    if (PyType_Ready(&_pypolkit_PolkitUnixUser_Type_obj) < 0)
        return;

    Py_INCREF(&_pypolkit_PolkitUnixUser_Type_obj);
    PyModule_AddObject(m, "UnixUser",
                       (PyObject *)&_pypolkit_PolkitUnixUser_Type_obj);
}


