/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_unixprocess.h"
#include "typeobjects/unixprocess.h"

void _pypolkit_PolkitUnixProcess_dealloc(_pypolkit_PolkitUnixProcess *self) 
{
    PyObject_GC_UnTrack(self);
    Py_CLEAR(self->pid);
    self->pid = NULL;
    Py_CLEAR(self->start_time);
    self->start_time = NULL;
    Py_CLEAR(self->uid);
    self->uid = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitUnixProcess_clear(_pypolkit_PolkitUnixProcess *self) 
{
    Py_CLEAR(self->pid);
    self->pid = NULL;
    Py_CLEAR(self->start_time);
    self->start_time = NULL;
    Py_CLEAR(self->uid);
    self->uid = NULL;
    return 0;
}

int _pypolkit_PolkitUnixProcess_traverse(_pypolkit_PolkitUnixProcess *self, visitproc visit, void *arg)
{
    Py_VISIT(self->pid);
    Py_VISIT(self->start_time);
    Py_VISIT(self->uid);
    return 0;
}

int _pypolkit_PolkitUnixProcess_init(_pypolkit_PolkitUnixProcess *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject *_pypolkit_PolkitUnixProcess_get(_pypolkit_PolkitUnixProcess *self, void *closure) 
{
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixProcess");
        return NULL;
    }

    if (!strcmp(member, "pid")) {
        return self->pid;
    }else if (!strcmp(member, "start_time")) {
        return self->start_time;
    }else if (!strcmp(member, "uid")) {
        return self->uid;
    } 
}

int _pypolkit_PolkitUnixProcess_set(_pypolkit_PolkitUnixProcess *self, PyObject *value, void *closure) 
{
    char *member = (char *) closure;
    PyObject *prop = NULL;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnix");
        return NULL;
    }

    if(!PyArg_ParseTuple(value, "i", &prop)){
        return NULL;
    }
    if(prop == NULL){
        return NULL;
    }

    if (!strcmp(member, "pid")) {
        self->pid = prop;
    }else if (!strcmp(member, "start_time")) {
        self->start_time = prop;
    }else if (!strcmp(member, "uid")) {
        self->uid = prop;
    } 

    return 0;
}

PyObject * pypolkit_polkit_unix_process_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitUnixProcess *unixprocess = NULL;
    gint pid;

    if(!PyArg_ParseTuple(args, "i", &pid)){
        return NULL;
    }

    unixprocess = (PolkitUnixProcess *)polkit_unix_process_new(pid);
    if(unixprocess){
        ret = PolkitUnixProcess2_pypolkit_PolkitUnixProcess(unixprocess);
    }
    
    g_object_unref(unixprocess);

    return ret;
}
