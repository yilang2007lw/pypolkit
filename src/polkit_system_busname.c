/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_system_busname.h"
#include "typeobjects/system_busname.h"

void _pypolkit_PolkitSystemBusName_dealloc(_pypolkit_PolkitSystemBusName *self) 
{
    PyObject_GC_UnTrack(self);
    self->name = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitSystemBusName_traverse(_pypolkit_PolkitSystemBusName *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitSystemBusName_clear(_pypolkit_PolkitSystemBusName *self) 
{

    return 0;
}

int _pypolkit_PolkitSystemBusName_init(_pypolkit_PolkitSystemBusName *self, PyObject *args, PyObject *kwds) 
{

    return 0;
}

PyObject *_pypolkit_PolkitSystemBusName_get(_pypolkit_PolkitSystemBusName *self, void *closure) 
{
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitSystemBusName");
        return NULL;
    }

    if (!strcmp(member, "name")) {
        return Py_BuildValue("s", self->name);
    } 
}

int _pypolkit_PolkitSystemBusName_set(_pypolkit_PolkitSystemBusName *self, PyObject *value, void *closure) {
    char *member = (char *) closure;
    char *name = NULL;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitSystemBusName");
        return NULL;
    }

    if(!PyArg_ParseTuple(value, "s", &name)){
        return NULL;
    }

    if(name == NULL){
        return NULL;
    }

    if (!strcmp(member, "name")) {
        return NULL;
    } 
    
    self->name = name;

    return 0;
}

PyObject * pypolkit_polkit_system_bus_name_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitSystemBusName *system_busname = NULL;

    const gchar *name;

    if(!PyArg_ParseTuple(args, "s", &name)){
        return NULL;
    }
    
    system_busname = polkit_system_bus_name_new(name);
    if(system_busname){
        ret = PolkitSystemBusName2_pypolkit_PolkitSystemBusName(system_busname);
    }

    g_object_unref(system_busname);

    return ret;
}

PyObject * pypolkit_polkit_system_bus_name_get_process(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitSubject *subject = NULL;
    PolkitSystemBusName *systembusname = NULL;
    systembusname = _pypolkit_PolkitSystemBusName2PolkitSystemBusName(s);
    if(systembusname == NULL){
        return NULL;
    }

    subject = polkit_system_bus_name_get_process_sync(systembusname, NULL, NULL);
    if(subject){
        ret = PolkitSubject2_pypolkit_PolkitSubject(subject);
    }

    g_object_unref(subject);
    g_object_unref(systembusname);

    return ret;
}
