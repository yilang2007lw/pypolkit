/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_unixnetgroup.h"
#include "typeobjects/unixnetgroup.h"

void _pypolkit_PolkitUnixNetGroup_dealloc(_pypolkit_PolkitUnixNetGroup *self) 
{
    PyObject_GC_UnTrack(self);
    self->name = NULL;
    PyObject_GC_Del(self);
}

int _pypolkit_PolkitUnixNetGroup_traverse(_pypolkit_PolkitUnixNetGroup *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitUnixNetGroup_clear(_pypolkit_PolkitUnixNetGroup *self) 
{
    return 0;
}

int _pypolkit_PolkitUnixNetGroup_init(_pypolkit_PolkitUnixNetGroup *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject *_pypolkit_PolkitUnixNetGroup_get(_pypolkit_PolkitUnixNetGroup *self, void *closure) 
{
    char *member = (char *) closure;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixNetGroup");
        return NULL;
    }

    if (!strcmp(member, "name")) {
        return Py_BuildValue("s", self->name);
    } 
}

int _pypolkit_PolkitUnixNetGroup_set(_pypolkit_PolkitUnixNetGroup *self, PyObject *value, void *closure) 
{
    char *member = (char *) closure;
    char *name = NULL;

    if (member == NULL) {
        PyErr_SetString(PyExc_TypeError, "Empty _pypolkit.PolkitUnixNetGroup");
        return NULL;
    }

    if(!PyArg_ParseTuple(value, "s", &name)){
        return NULL;
    }

    if(name == NULL){
        return NULL;
    }

    if (!strcmp(member, "name")) {
        return NULL;
    } 
    
    self->name = name;

    return 0;
}


PyObject * pypolkit_polkit_unix_netgroup_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitUnixNetgroup *group = NULL;
    const gchar *name;

    if(!PyArg_ParseTuple(args, "s", &name)){
        return NULL;
    }

    group = polkit_unix_netgroup_new(name);
    if(group){
        ret = PolkitUnixNetgroup2_pypolkit_PolkitUnixNetGroup(group);
    }
    
    g_object_unref(group);

    return ret;
}
