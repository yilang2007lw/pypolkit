#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 ~ 2013 Deepin, Inc.
#               2012 ~ 2013 Long Wei
#
# Author:     Long Wei <yilang2007lw@gmail.com>
# Maintainer: Long Wei <yilang2007lw@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pypolkit


if __name__ == "__main__":
    # authority = pypolkit.authority_new()
    # action_description = pypolkit.action_description_new("org.freedesktop.accounts.user-administration")
    # action_description.get_icon_name()
    # details = pypolkit.details_new()
    # permission = pypolkit.permission_new("org.freedesktop.accounts.user-administration")
    # print permission.allowed
    # print permission.action_id
    # print permission.subject
    # print permission.can_acquire
    # print permission.can_release

    # authorization_result = pypolkit.authorization_result_new(1, 1)
    # pypolkit.unix_session_new("test")
    # pypolkit.unix_process_new(1)
    pypolkit.system_busname_new("org.freedesktop.NetworkManager")
    # bus.get_process()
