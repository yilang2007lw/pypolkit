/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>

#include "convert.h"
#include "polkit_unixuser.h"
#include "typeobjects/unixuser.h"

void _pypolkit_PolkitUnixUser_dealloc(_pypolkit_PolkitUnixUser *self) 
{
    PyObject_GC_UnTrack(self);

    PyObject_GC_Del(self);
}

int _pypolkit_PolkitUnixUser_traverse(_pypolkit_PolkitUnixUser *self, visitproc visit, void *arg)
{
    return 0;
}

int _pypolkit_PolkitUnixUser_clear(_pypolkit_PolkitUnixUser *self) 
{
    return 0;
}

int _pypolkit_PolkitUnixUser_init(_pypolkit_PolkitUnixUser *self, PyObject *args, PyObject *kwds) 
{
    return 0;
}

PyObject *_pypolkit_PolkitUnixUser_get(_pypolkit_PolkitUnixUser *self, void *closure) 
{
    return Py_True;
}

int _pypolkit_PolkitUnixUser_set(_pypolkit_PolkitUnixUser *self, PyObject *value, void *closure) 
{
    return 0;
}

PyObject * pypolkit_polkit_unix_user_new(PyObject *s, PyObject *args)
{
    PyObject *ret = NULL;
    PolkitUnixUser *unixuser = NULL;
    gint uid;

    if(!PyArg_ParseTuple(args, "i", &uid)){
        return NULL;
    }

    unixuser = (PolkitUnixUser *)polkit_unix_user_new(uid);
    if(unixuser){
        ret = PolkitUnixUser2_pypolkit_PolkitUnixUser(unixuser);
    }

    g_object_unref(unixuser);
    
    return ret;
}

PyObject * pypolkit_polkit_unix_user_get_name(PyObject *s, PyObject *args)
{
    const gchar *user;
    PolkitUnixUser *unixuser;
    unixuser = _pypolkit_PolkitUnixUser2PolkitUnixUser(s);

    if(unixuser){
        return -1;
    }
    user = polkit_unix_user_get_name(unixuser);

    return Py_BuildValue("s", user);
}
