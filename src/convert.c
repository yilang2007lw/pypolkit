/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>
#include <polkit/polkit.h>
#include "convert.h"

PolkitAuthority *_pypolkit_PolkitAuthority2PolkitAuthority(PyObject *s)
{
    PolkitAuthority *ret = NULL;
    _pypolkit_PolkitAuthority *authority = (_pypolkit_PolkitAuthority *)s;

    if(authority == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit authority from python to c");
        return NULL;
    }

    ret = polkit_authority_get_sync(NULL, NULL);
    if(ret == NULL){
        return (PolkitAuthority *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitAuthority *PolkitAuthority2_pypolkit_PolkitAuthority(PolkitAuthority *authority)
{
    _pypolkit_PolkitAuthority *ret = NULL;
    PyObject *args = NULL;

    if (authority == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit authority from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitAuthority *) _pypolkit_PolkitAuthority_Type_obj.tp_new(&_pypolkit_PolkitAuthority_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitAuthority *)PyErr_NoMemory();
    }
    
    args = Py_BuildValue("sssi", polkit_authority_get_backend_name(authority),
                         polkit_authority_get_backend_version(authority),
                         polkit_authority_get_owner(authority),
                         polkit_authority_get_backend_features(authority));
    
    if(args == NULL){
        goto error;
    }

    if((_pypolkit_PolkitAuthority_Type_obj.tp_init((PyObject *)ret, args, NULL))){
        goto error;
    }

    Py_DECREF(args);

    return ret;
error:
    Py_XDECREF(ret);
    Py_DECREF(ret);
    return NULL;
}

PolkitAuthorizationResult *_pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(PyObject *s)
{
    PolkitAuthorizationResult *ret = NULL;

    _pypolkit_PolkitAuthorizationResult *result = (_pypolkit_PolkitAuthorizationResult *)s;
    if(result == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit authorization result from python to c");
        return NULL;
    }

    ret = polkit_authorization_result_new(result->is_authorized, result->is_challenge, result->details);
    if(ret == NULL){
        return (PolkitAuthorizationResult *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitAuthorizationResult *PolkitAuthorizationResult2_pypolkit_PolkitAuthorizationResult(PolkitAuthorizationResult *authorization_result)
{
    _pypolkit_PolkitAuthorizationResult *ret = NULL;

    if (authorization_result == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit authorization result from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitAuthorizationResult *) _pypolkit_PolkitAuthorizationResult_Type_obj.tp_new(&_pypolkit_PolkitAuthorizationResult_Type_obj, NULL, NULL);
    if (!ret) {
        goto error;
    }

    return ret;

error:
    Py_DECREF(ret);
    return NULL;
}

PolkitDetails *_pypolkit_PolkitDetails2PolkitDetails(PyObject *s)
{
    PolkitDetails *ret = NULL;

    _pypolkit_PolkitDetails *details = (_pypolkit_PolkitDetails *)s;
    if(details == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit details from python to c");
        return NULL;
    }

    ret = polkit_details_new();
    if(ret == NULL){
        return (PolkitDetails *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitDetails *PolkitDetails2_pypolkit_PolkitDetails(PolkitDetails *details)
{
    _pypolkit_PolkitDetails *ret = NULL;

    if (details == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit details from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitDetails *) _pypolkit_PolkitDetails_Type_obj.tp_new(&_pypolkit_PolkitDetails_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitDetails *)PyErr_NoMemory();
    }

    return ret;

error:
    Py_DECREF(ret);
    return NULL;
}

PolkitActionDescription *_pypolkit_PolkitActionDescription2PolkitActionDescription(PyObject *s)
{
    PolkitActionDescription *ret = NULL;

    _pypolkit_PolkitActionDescription *action_description = (_pypolkit_PolkitActionDescription *)s;
    if(action_description == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit action description from python to c");
        return NULL;
    }

    /* need fix this */
    ret = polkit_action_description_new(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

    if(!ret){
        return (PolkitActionDescription *)PyErr_NoMemory();
    }    

    return ret;
}

_pypolkit_PolkitActionDescription *PolkitActionDescription2_pypolkit_PolkitActionDescription(PolkitActionDescription *action_description)
{
    _pypolkit_PolkitActionDescription *ret = NULL;

    if (action_description == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit action description from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitActionDescription *) _pypolkit_PolkitActionDescription_Type_obj.tp_new(&_pypolkit_PolkitActionDescription_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitActionDescription *)PyErr_NoMemory();
    }

    return ret;
error:
    Py_DECREF(ret);
    return NULL;
}

PolkitTemporaryAuthorization *_pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(PyObject *s)
{
    PolkitTemporaryAuthorization *ret = NULL;

    _pypolkit_PolkitTemporaryAuthorization *temporary_authorization = (_pypolkit_PolkitTemporaryAuthorization *)s;
    if(temporary_authorization == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit temporary authorization from python to c");
        return NULL;
    }

    /* fix this */
    ret = polkit_temporary_authorization_new(NULL, NULL, NULL, NULL, NULL);

    if(ret == NULL){
        return (PolkitTemporaryAuthorization *)PyErr_NoMemory();
    }
    
    return ret;
}

_pypolkit_PolkitTemporaryAuthorization *PolkitTemporaryAuthorization2_pypolkit_PolkitTemporaryAuthorization(PolkitTemporaryAuthorization *temporary_authorization)
{
    _pypolkit_PolkitTemporaryAuthorization *ret = NULL;

    if (temporary_authorization == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit temporary authorization from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitTemporaryAuthorization *) _pypolkit_PolkitTemporaryAuthorization_Type_obj.tp_new(&_pypolkit_PolkitTemporaryAuthorization_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitTemporaryAuthorization *)PyErr_NoMemory();
    }

    return ret;
error:
    Py_DECREF(ret);
    return NULL;
}

PolkitPermission *_pypolkit_PolkitPermission2PolkitPermission(PyObject *s)
{
    PolkitPermission *ret = NULL;

    _pypolkit_PolkitPermission *polkit_permission = (_pypolkit_PolkitPermission *)s;
    if(polkit_permission == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit permission from python to c");
        return NULL;
    }
    
    ret = polkit_permission_new_sync(polkit_permission->action_id, polkit_permission->subject, NULL, NULL);
    if(ret == NULL){
        return (PolkitPermission *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitPermission *PolkitPermission2_pypolkit_PolkitPermission(PolkitPermission *permission)
{
    _pypolkit_PolkitPermission *ret = NULL;
    PyObject *args = NULL;

    if (permission == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit permission from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitPermission *) _pypolkit_PolkitPermission_Type_obj.tp_new(&_pypolkit_PolkitPermission_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitPermission *)PyErr_NoMemory();
    }

    args = Py_BuildValue("sO", polkit_permission_get_action_id(permission), polkit_permission_get_subject(permission));
    if(args == NULL){
        goto error;
    }

    if(_pypolkit_PolkitPermission_Type_obj.tp_init((PyObject *)ret, args, NULL)){
        goto error;
    }

    Py_DECREF(args);

    return ret;

error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitSubject *_pypolkit_PolkitSubject2PolkitSubject(PyObject *s)
{
    PolkitSubject *ret = NULL;
    _pypolkit_PolkitSubject *polkit_subject = (_pypolkit_PolkitSubject *)s;

    if(polkit_subject == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit subject from python to c");
        return NULL;
    }
    
    /* fix this */
    ret = polkit_subject_from_string(NULL, NULL);
    if(ret == NULL){
        return (PolkitSubject *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitSubject *PolkitSubject2_pypolkit_PolkitSubject(PolkitSubject *subject)
{
    _pypolkit_PolkitSubject *ret = NULL;

    if (subject == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit subject from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitSubject *) _pypolkit_PolkitSubject_Type_obj.tp_new(&_pypolkit_PolkitSubject_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitSubject *)PyErr_NoMemory();
    }

    return ret;
error:
    Py_DECREF(ret);
    return NULL;
}

PolkitUnixProcess *_pypolkit_PolkitUnixProces2PolkitUnixProcess(PyObject *s)
{
    PolkitUnixProcess *ret = NULL;
    _pypolkit_PolkitUnixProcess *unix_process = (_pypolkit_PolkitUnixProcess *)s;

    if(unix_process == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit unix process from python to c");
        return NULL;
    }

    ret = polkit_unix_process_new(unix_process->pid);
    if(ret == NULL){
        return (PolkitUnixProcess *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitUnixProcess *PolkitUnixProcess2_pypolkit_PolkitUnixProcess(PolkitUnixProcess *unixprocess)
{
    _pypolkit_PolkitUnixProcess *ret = NULL;
    PyObject *args;

    if (unixprocess == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit unix process from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitUnixProcess *) _pypolkit_PolkitUnixProcess_Type_obj.tp_new(&_pypolkit_PolkitUnixProcess_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitUnixProcess *)PyErr_NoMemory();
    }

    args = Py_BuildValue("sls", polkit_unix_process_get_pid(unixprocess), 
                         polkit_unix_process_get_start_time(unixprocess),
                         polkit_unix_process_get_uid(unixprocess));
    if(args == NULL){
        goto error;
    }
    
    if((_pypolkit_PolkitUnixProcess_Type_obj.tp_init((PyObject *)ret, args, NULL))){
        goto error;
    }

    Py_DECREF(args);

    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitUnixSession *_pypolkit_PolkitUnixSession2PolkitUnixSession(PyObject *s)
{
    PolkitUnixSession *ret = NULL;
    _pypolkit_PolkitUnixSession *unix_session = (_pypolkit_PolkitUnixSession *)s;

    if(unix_session == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit unix session from python to c");
        return NULL;
    }

    ret = polkit_unix_session_new(unix_session->session_id);
    if(ret == NULL){
        return (PolkitUnixSession *)PyErr_NoMemory();
    }
    
    return ret;
}

_pypolkit_PolkitUnixSession *PolkitUnixSession2_pypolkit_PolkitUnixSession(PolkitUnixSession *unixsession)
{
    _pypolkit_PolkitUnixSession *ret = NULL;
    PyObject *args;

    if (unixsession == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit unix session from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitUnixSession *) _pypolkit_PolkitUnixSession_Type_obj.tp_new(&_pypolkit_PolkitUnixSession_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitUnixSession *)PyErr_NoMemory();
    }

    args = Py_BuildValue("s", polkit_unix_session_get_session_id(unixsession));
    if(args == NULL){
        goto error;
    }
    
    Py_DECREF(args);
    
    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitSystemBusName *_pypolkit_PolkitSystemBusName2PolkitSystemBusName(PyObject *s)
{
    PolkitSystemBusName *ret = NULL;
    _pypolkit_PolkitSystemBusName *system_busname = (_pypolkit_PolkitSystemBusName *)s;

    if(system_busname == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit system bus name from python to c");
        return NULL;
    }

    ret = polkit_system_bus_name_new(system_busname->name);
    if(ret == NULL){
        return (PolkitSystemBusName *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitSystemBusName *PolkitSystemBusName2_pypolkit_PolkitSystemBusName(PolkitSystemBusName *system_busname)
{
    _pypolkit_PolkitSystemBusName *ret = NULL;
    PyObject *args;

    if (system_busname == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit system bus name from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitSystemBusName *) _pypolkit_PolkitSystemBusName_Type_obj.tp_new(&_pypolkit_PolkitSystemBusName_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitSystemBusName *)PyErr_NoMemory();
    }

    args = Py_BuildValue("s", polkit_system_bus_name_get_name(system_busname));
    if(args == NULL){
        goto error;
    }

    if(_pypolkit_PolkitSystemBusName_Type_obj.tp_init((PyObject *)ret, args ,NULL)){
        goto error;
    }
    Py_DECREF(args);

    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitIdentity *_pypolkit_PolkitIdentity2PolkitIdentity(PyObject *s)
{
    PolkitIdentity *ret = NULL;
    _pypolkit_PolkitIdentity *polkit_identity = (_pypolkit_PolkitIdentity *)s;

    if(polkit_identity == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit identity from python to c");
        return NULL;
    }

    /* fix this */
    ret = polkit_identity_from_string(NULL, NULL);
    if(ret == NULL){
        return (PolkitIdentity *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitIdentity *PolkitIdentity2_pypolkit_PolkitIdentity(PolkitIdentity *identity)
{
    _pypolkit_PolkitIdentity *ret = NULL;

    if (identity == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit identity from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitIdentity *) _pypolkit_PolkitIdentity_Type_obj.tp_new(&_pypolkit_PolkitIdentity_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitIdentity *)PyErr_NoMemory();
    }

    return ret;
error:
    Py_DECREF(ret);
    return NULL;
}

PolkitUnixUser *_pypolkit_PolkitUnixUser2PolkitUnixUser(PyObject *s)
{
    PolkitUnixUser *ret;
    _pypolkit_PolkitUnixUser *polkit_unixuser = (_pypolkit_PolkitUnixUser *)s;

    if(polkit_unixuser == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit unix user from python to c");
        return NULL;
    }

    ret = polkit_unix_user_new(polkit_unixuser->uid);
    if(ret == NULL){
        return (PolkitUnixUser *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitUnixUser *PolkitUnixUser2_pypolkit_PolkitUnixUser(PolkitUnixUser *unixuser)
{
    _pypolkit_PolkitUnixUser *ret = NULL;
    PyObject *args;

    if (unixuser == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit unix user from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitUnixUser *) _pypolkit_PolkitUnixUser_Type_obj.tp_new(&_pypolkit_PolkitUnixUser_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitUnixUser *)PyErr_NoMemory();
    }

    args = Py_BuildValue("i", polkit_unix_user_get_uid(unixuser));
    if(args == NULL){
        goto error;
    }

    if(_pypolkit_PolkitUnixUser_Type_obj.tp_init((PyObject *)ret, args, NULL)){
        goto error;
    }

    Py_DECREF(args);
    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitUnixGroup *_pypolkit_PolkitUnixGroup2PolkitUnixGroup(PyObject *s)
{
    PolkitUnixGroup *ret;
    _pypolkit_PolkitUnixGroup *polkit_unixgroup = (_pypolkit_PolkitUnixGroup *)s;

    if(polkit_unixgroup == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit unix group from python to c");
        return NULL;
    }

    ret = polkit_unix_group_new(polkit_unixgroup->gid);
    if(ret == NULL){
        return (PolkitUnixGroup *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitUnixGroup *PolkitUnixGroup2_pypolkit_PolkitUnixGroup(PolkitUnixGroup *unixgroup)
{
    _pypolkit_PolkitUnixGroup *ret = NULL;
    PyObject *args;

    if (unixgroup == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit unix group from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitUnixGroup *) _pypolkit_PolkitUnixGroup_Type_obj.tp_new(&_pypolkit_PolkitUnixGroup_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitUnixGroup *)PyErr_NoMemory();
    }

    args = Py_BuildValue("i", polkit_unix_group_get_gid(unixgroup));
    if(!args){
        goto error;
    }

    if(_pypolkit_PolkitUnixGroup_Type_obj.tp_init((PyObject *)ret, args, NULL)){
        goto error;
    }
    
    Py_DECREF(args);

    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

PolkitUnixNetgroup *_pypolkit_PolkitUnixNetGroup2PolkitUnixNetgroup(PyObject *s)
{
    PolkitUnixNetgroup *ret = NULL;

    _pypolkit_PolkitUnixNetGroup *unixnetgroup = (_pypolkit_PolkitUnixNetGroup *)s;
    if(unixnetgroup == NULL){
        PyErr_SetString(PyExc_TypeError, "convert polkit unix net group from python to c");
        return NULL;
    }

    ret = polkit_unix_netgroup_new(unixnetgroup->name);
    if(ret == NULL){
        return (PolkitUnixNetgroup *)PyErr_NoMemory();
    }

    return ret;
}

_pypolkit_PolkitUnixNetGroup *PolkitUnixNetgroup2_pypolkit_PolkitUnixNetGroup(PolkitUnixNetgroup *unixnetgroup)
{
    _pypolkit_PolkitUnixNetGroup *ret = NULL;
    PyObject *args;

    if (unixnetgroup == NULL) {
        PyErr_SetString(PyExc_TypeError, "convert polkit unix net group from c to python");
        return NULL;
    }

    ret = (_pypolkit_PolkitUnixNetGroup *) _pypolkit_PolkitUnixNetGroup_Type_obj.tp_new(&_pypolkit_PolkitUnixNetGroup_Type_obj, NULL, NULL);
    if (!ret) {
        return (_pypolkit_PolkitUnixNetGroup *)PyErr_NoMemory();
    }

    args = Py_BuildValue("s", polkit_unix_netgroup_get_name(unixnetgroup));
    if(args == NULL){
        goto error;
    }

    if(_pypolkit_PolkitUnixNetGroup_Type_obj.tp_init((PyObject *)ret, args, NULL)){
        goto error;
    }

    Py_DECREF(args);
    return ret;
error:
    Py_XDECREF(args);
    Py_DECREF(ret);
    return NULL;
}

