/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_SYSTEM_BUS_NAME_H
#define PYPOLKIT_POLKIT_SYSTEM_BUS_NAME_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    gchar *name;
}_pypolkit_PolkitSystemBusName;

void _pypolkit_PolkitSystemBusName_dealloc(_pypolkit_PolkitSystemBusName *);
int _pypolkit_PolkitSystemBusName_traverse(_pypolkit_PolkitSystemBusName *, visitproc, void *);
int _pypolkit_PolkitSystemBusName_clear(_pypolkit_PolkitSystemBusName *);
int _pypolkit_PolkitSystemBusName_init(_pypolkit_PolkitSystemBusName *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitSystemBusName_get(_pypolkit_PolkitSystemBusName *, void *);
int _pypolkit_PolkitSystemBusName_set(_pypolkit_PolkitSystemBusName *, PyObject *, void *);

PyObject * pypolkit_polkit_system_bus_name_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_system_bus_name_get_process(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitSystemBusName_Type_obj;

#endif
