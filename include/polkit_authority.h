/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_AUTHORITY_H
#define PYPOLKIT_POLKIT_AUTHORITY_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD

    gchar *backend_name;
    gchar *backend_version;
    gchar *owner;
    int backend_features;

    PolkitAuthority *polkitauthority;    
}_pypolkit_PolkitAuthority;

void _pypolkit_PolkitAuthority_dealloc(_pypolkit_PolkitAuthority *);
int _pypolkit_PolkitAuthority_traverse(_pypolkit_PolkitAuthority *, visitproc, void *);
int _pypolkit_PolkitAuthority_clear(_pypolkit_PolkitAuthority *);
int _pypolkit_PolkitAuthority_init(_pypolkit_PolkitAuthority *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitAuthority_get(_pypolkit_PolkitAuthority *, void *);
int _pypolkit_PolkitAuthority_set(_pypolkit_PolkitAuthority *, PyObject *, void *);

PyObject * pypolkit_polkit_authority_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_check_authorization(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_enumerate_actions(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_register_authentication_agent(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_register_authentication_agent_with_options(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_unregister_authentication_agent(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_authentication_agent_response(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_enumerate_temporary_authorizations(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_revoke_temporary_authorizations(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authority_revoke_temporary_authorizations_by_id(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitAuthority_Type_obj;

#endif
