/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_PERMISSION_H
#define PYPOLKIT_POLKIT_PERMISSION_H

#include <Python.h>

#include <polkit/polkit.h>
#include <gio/gio.h>

typedef struct{
    PyObject_HEAD

    gchar *action_id;    
    PyObject *subject;
    gboolean allowed;
    gboolean can_acquire;
    gboolean can_release;

}_pypolkit_PolkitPermission;

void _pypolkit_PolkitPermission_dealloc(_pypolkit_PolkitPermission *);
int _pypolkit_PolkitPermission_traverse(_pypolkit_PolkitPermission *, visitproc, void *);
int _pypolkit_PolkitPermission_clear(_pypolkit_PolkitPermission *);
int _pypolkit_PolkitPermission_init(_pypolkit_PolkitPermission *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitPermission_get(_pypolkit_PolkitPermission *, void *);
int _pypolkit_PolkitPermission_set(_pypolkit_PolkitPermission *, PyObject *, void *);

PyObject * pypolkit_polkit_permission_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_permission_acquire(PyObject *, PyObject *);
PyObject * pypolkit_polkit_permission_release(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitPermission_Type_obj;
#endif
