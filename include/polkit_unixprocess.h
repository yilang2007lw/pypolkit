/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_UNIX_PROCESS_H
#define PYPOLKIT_POLKIT_UNIX_PROCESS_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    PyObject *pid;
    PyObject *start_time;
    PyObject *uid;
}_pypolkit_PolkitUnixProcess;

void _pypolkit_PolkitUnixProcess_dealloc(_pypolkit_PolkitUnixProcess *);
int _pypolkit_PolkitUnixProcess_traverse(_pypolkit_PolkitUnixProcess *, visitproc, void *);
int _pypolkit_PolkitUnixProcess_clear(_pypolkit_PolkitUnixProcess *);
int _pypolkit_PolkitUnixProcess_init(_pypolkit_PolkitUnixProcess *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitUnixProcess_get(_pypolkit_PolkitUnixProcess *, void *);
int _pypolkit_PolkitUnixProcess_set(_pypolkit_PolkitUnixProcess *, PyObject *, void *);

extern PyTypeObject _pypolkit_PolkitUnixProcess_Type_obj;

PyObject * pypolkit_polkit_unix_process_new(PyObject *, PyObject *);

#endif
