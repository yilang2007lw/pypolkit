/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_UNIX_SESSION_H
#define PYPOLKIT_POLKIT_UNIX_SESSION_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    gint pid;    
    gchar *session_id;
}_pypolkit_PolkitUnixSession;

void _pypolkit_PolkitUnixSession_dealloc(_pypolkit_PolkitUnixSession *);
int _pypolkit_PolkitUnixSession_traverse(_pypolkit_PolkitUnixSession *, visitproc, void *);
int _pypolkit_PolkitUnixSession_clear(_pypolkit_PolkitUnixSession *);
int _pypolkit_PolkitUnixSession_init(_pypolkit_PolkitUnixSession *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitUnixSession_get(_pypolkit_PolkitUnixSession *, void *);
int _pypolkit_PolkitUnixSession_set(_pypolkit_PolkitUnixSession *, PyObject *, void *);

extern PyTypeObject _pypolkit_PolkitUnixSession_Type_obj;

PyObject * pypolkit_polkit_unix_session_new(PyObject *, PyObject *);

#endif
