/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_CONVERT_H
#define PYPOLKIT_CONVERT_H

#include <polkit/polkit.h>
#include "polkit_authority.h"
#include "polkit_authorization_result.h"
#include "polkit_details.h"
#include "polkit_action_description.h"
#include "polkit_temporary_authorization.h"
#include "polkit_permission.h"
#include "polkit_subject.h"
#include "polkit_unixprocess.h"
#include "polkit_unixsession.h"
#include "polkit_system_busname.h"
#include "polkit_identity.h"
#include "polkit_unixuser.h"
#include "polkit_unixgroup.h"
#include "polkit_unixnetgroup.h"

PolkitAuthority *_pypolkit_PolkitAuthority2PolkitAuthority(PyObject *);

_pypolkit_PolkitAuthority *PolkitAuthority2_pypolkit_PolkitAuthority(PolkitAuthority *);

PolkitAuthorizationResult *_pypolkit_PolkitAuthorizationResult2PolkitAuthorizationResult(PyObject *);

_pypolkit_PolkitAuthorizationResult *PolkitAuthorizationResult2_pypolkit_PolkitAuthorizationResult(PolkitAuthorizationResult *);

PolkitDetails *_pypolkit_PolkitDetails2PolkitDetails(PyObject *);

_pypolkit_PolkitDetails *PolkitDetails2_pypolkit_PolkitDetails(PolkitDetails *);

PolkitActionDescription *_pypolkit_PolkitActionDescription2PolkitActionDescription(PyObject *);

_pypolkit_PolkitActionDescription *PolkitActionDescription2_pypolkit_PolkitActionDescription(PolkitActionDescription *);

PolkitTemporaryAuthorization *_pypolkit_PolkitTemporaryAuthorization2PolkitTemporaryAuthorization(PyObject *);

_pypolkit_PolkitTemporaryAuthorization *PolkitTemporaryAuthorization2_pypolkit_PolkitTemporaryAuthorization(PolkitTemporaryAuthorization *);

PolkitPermission *_pypolkit_PolkitPermission2PolkitPermission(PyObject *);

_pypolkit_PolkitPermission *PolkitPermission2_pypolkit_PolkitPermission(PolkitPermission *);

PolkitSubject *_pypolkit_PolkitSubject2PolkitSubject(PyObject *);

_pypolkit_PolkitSubject *PolkitSubject2_pypolkit_PolkitSubject(PolkitSubject *);

PolkitUnixProcess *_pypolkit_PolkitUnixProces2PolkitUnixProcess(PyObject *);

_pypolkit_PolkitUnixProcess *PolkitUnixProcess2_pypolkit_PolkitUnixProcess(PolkitUnixProcess *);

PolkitUnixSession *_pypolkit_PolkitUnixSession2PolkitUnixSession(PyObject *);

_pypolkit_PolkitUnixSession *PolkitUnixSession2_pypolkit_PolkitUnixSession(PolkitUnixSession *);

PolkitSystemBusName *_pypolkit_PolkitSystemBusName2PolkitSystemBusName(PyObject *);

_pypolkit_PolkitSystemBusName *PolkitSystemBusName2_pypolkit_PolkitSystemBusName(PolkitSystemBusName *);

PolkitIdentity *_pypolkit_PolkitIdentity2PolkitIdentity(PyObject *);

_pypolkit_PolkitIdentity *PolkitIdentity2_pypolkit_PolkitIdentity(PolkitIdentity *);

PolkitUnixUser *_pypolkit_PolkitUnixUser2PolkitUnixUser(PyObject *);

_pypolkit_PolkitUnixUser *PolkitUnixUser2_pypolkit_PolkitUnixUser(PolkitUnixUser *);

PolkitUnixGroup *_pypolkit_PolkitUnixGroup2PolkitUnixGroup(PyObject *);

_pypolkit_PolkitUnixGroup *PolkitUnixGroup2_pypolkit_PolkitUnixGroup(PolkitUnixGroup *);

PolkitUnixNetgroup *_pypolkit_PolkitUnixNetGroup2PolkitUnixNetgroup(PyObject *);

_pypolkit_PolkitUnixNetGroup *PolkitUnixNetgroup2_pypolkit_PolkitUnixNetGroup(PolkitUnixNetgroup *);


#endif
