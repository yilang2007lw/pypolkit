/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_UNIX_USER_H
#define PYPOLKIT_POLKIT_UNIX_USER_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    gint uid;    
}_pypolkit_PolkitUnixUser;

void _pypolkit_PolkitUnixUser_dealloc(_pypolkit_PolkitUnixUser *);
int _pypolkit_PolkitUnixUser_traverse(_pypolkit_PolkitUnixUser *, visitproc, void *);
int _pypolkit_PolkitUnixUser_clear(_pypolkit_PolkitUnixUser *);
int _pypolkit_PolkitUnixUser_init(_pypolkit_PolkitUnixUser *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitUnixUser_get(_pypolkit_PolkitUnixUser *, void *);
int _pypolkit_PolkitUnixUser_set(_pypolkit_PolkitUnixUser *, PyObject *, void *);

PyObject * pypolkit_polkit_unix_user_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_unix_user_get_name(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitUnixUser_Type_obj;

#endif
