/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_AUTHORIZATION_RESULT_H
#define PYPOLKIT_POLKIT_AUTHORIZATION_RESULT_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    gboolean is_authorized;
    gboolean is_challenge;
    PyObject *details;

}_pypolkit_PolkitAuthorizationResult;

void _pypolkit_PolkitAuthorizationResult_dealloc(_pypolkit_PolkitAuthorizationResult *);
int _pypolkit_PolkitAuthorizationResult_traverse(_pypolkit_PolkitAuthorizationResult *, visitproc, void *);
int _pypolkit_PolkitAuthorizationResult_clear(_pypolkit_PolkitAuthorizationResult *);
int _pypolkit_PolkitAuthorizationResult_init(_pypolkit_PolkitAuthorizationResult *, PyObject *, PyObject *);

PyObject * pypolkit_polkit_authorization_result_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_result_get_is_authorized(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_result_get_is_challenge(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_result_get_retains_authorization(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_result_get_temporary_authorization_id(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_result_get_dismissed(PyObject *, PyObject *);
PyObject * pypolkit_polkit_authorization_get_details(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitAuthorizationResult_Type_obj;

#endif
