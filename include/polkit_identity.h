/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_IDENTITY_H
#define PYPOLKIT_POLKIT_IDENTITY_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD

}_pypolkit_PolkitIdentity;

void _pypolkit_PolkitIdentity_dealloc(_pypolkit_PolkitIdentity *);
int _pypolkit_PolkitIdentity_traverse(_pypolkit_PolkitIdentity *, visitproc, void *);
int _pypolkit_PolkitIdentity_clear(_pypolkit_PolkitIdentity *);
int _pypolkit_PolkitIdentity_init(_pypolkit_PolkitIdentity *, PyObject *, PyObject *);

PyObject * pypolkit_polkit_identity_hash(PyObject *, PyObject *);
PyObject * pypolkit_polkit_identity_equal(PyObject *, PyObject *);
PyObject * pypolkit_polkit_identity_to_string(PyObject *, PyObject *);
PyObject * pypolkit_polkit_identity_from_string(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitIdentity_Type_obj;

#endif
