/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_UNIX_NET_GROUP_H
#define PYPOLKIT_POLKIT_UNIX_NET_GROUP_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD
    gchar *name;
}_pypolkit_PolkitUnixNetGroup;

void _pypolkit_PolkitUnixNetGroup_dealloc(_pypolkit_PolkitUnixNetGroup *);
int _pypolkit_PolkitUnixNetGroup_traverse(_pypolkit_PolkitUnixNetGroup *, visitproc, void *);
int _pypolkit_PolkitUnixNetGroup_clear(_pypolkit_PolkitUnixNetGroup *);
int _pypolkit_PolkitUnixNetGroup_init(_pypolkit_PolkitUnixNetGroup *, PyObject *, PyObject *);
PyObject *_pypolkit_PolkitUnixNetGroup_get(_pypolkit_PolkitUnixNetGroup *, void *);
int _pypolkit_PolkitUnixNetGroup_set(_pypolkit_PolkitUnixNetGroup *, PyObject *, void *);

extern PyTypeObject _pypolkit_PolkitUnixNetGroup_Type_obj;

PyObject * pypolkit_polkit_unix_netgroup_new(PyObject *, PyObject *);

#endif
