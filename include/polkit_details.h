/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_DETAILS_H
#define PYPOLKIT_POLKIT_DETAILS_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD

}_pypolkit_PolkitDetails;

void _pypolkit_PolkitDetails_dealloc(_pypolkit_PolkitDetails *);
int _pypolkit_PolkitDetails_traverse(_pypolkit_PolkitDetails *, visitproc, void *);
int _pypolkit_PolkitDetails_clear(_pypolkit_PolkitDetails *);
int _pypolkit_PolkitDetails_init(_pypolkit_PolkitDetails *, PyObject *, PyObject *);

PyObject * pypolkit_polkit_details_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_details_lookup(PyObject *, PyObject *);
PyObject * pypolkit_polkit_details_insert(PyObject *, PyObject *);
PyObject * pypolkit_polkit_details_get_keys(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitDetails_Type_obj;

#endif
