/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TYPEOBJECTS_POLKIT_SYSTEM_BUS_NAME_H
#define TYPEOBJECTS_POLKIT_SYSTEM_BUS_NAME_H

#include <Python.h>
#include <structmember.h>

static PyMemberDef _pypolkit_PolkitSystemBusName_members[] = {
    {"name", T_OBJECT, offsetof(_pypolkit_PolkitSystemBusName, name), RESTRICTED, "doc"},

    {NULL}
};

static PyMethodDef _pypolkit_PolkitSystemBusName_methods[] = {
    {"get_process", (PyCFunction) pypolkit_polkit_system_bus_name_get_process, METH_VARARGS, "doc" },

    {NULL}
};

static PyGetSetDef _pypolkit_PolkitSystemBusName_getset[] = {
    {"name", (getter) _pypolkit_PolkitSystemBusName_get, (setter) _pypolkit_PolkitSystemBusName_set, NULL, "doc"},

    {NULL}
};

PyTypeObject _pypolkit_PolkitSystemBusName_Type_obj = {
    PyObject_HEAD_INIT(&PyType_Type)
    .tp_name = "_pypolkit.PolkitSystemBusName",
    .tp_basicsize = sizeof(_pypolkit_PolkitSystemBusName),
    /* .tp_itemsize = XXX */
    .tp_dealloc = (destructor) _pypolkit_PolkitSystemBusName_dealloc,
    /* .tp_getattr = XXX */
    /* .tp_setattr = XXX */
    /* .tp_compare = (cmpfunc) _pypolkit_PolkitSystemBusName_compare, */
    /* .tp_repr = XXX */
    /* .tp_as_number = XXX */
    /* .tp_as_sequence = XXX */
    /* .tp_as_mapping = XXX */
    .tp_hash = PyObject_HashNotImplemented,
    .tp_call = NULL,
    /* .tp_str = (reprfunc) _pypolkit_PolkitSystemBusName_str, */
    .tp_getattro = PyObject_GenericGetAttr,
    .tp_setattro = PyObject_GenericSetAttr,
    /* .tp_as_buffer = XXX */
    .tp_flags = Py_TPFLAGS_HAVE_CLASS | Py_TPFLAGS_CHECKTYPES |
    Py_TPFLAGS_HAVE_GC | Py_TPFLAGS_BASETYPE,
    .tp_doc = "polkit PolkitSystemBusName doc",
    .tp_traverse = (traverseproc) _pypolkit_PolkitSystemBusName_traverse,
    .tp_clear = (inquiry) _pypolkit_PolkitSystemBusName_clear,
    /* .tp_richcompare = (richcmpfunc) _pypolkit_PolkitSystemBusName_richcompare, */
    /* .tp_weaklistoffset = XXX */
    /* .tp_iter = XXX */
    /* .tp_iternext = XXX */
    .tp_methods = _pypolkit_PolkitSystemBusName_methods,
    .tp_members = _pypolkit_PolkitSystemBusName_members,
    .tp_getset = _pypolkit_PolkitSystemBusName_getset,
    .tp_base = NULL,
    .tp_dict = NULL,
    /* .tp_descr_get = XXX */
    /* .tp_descr_set = XXX */
    /* .tp_dictoffset = XXX */
    .tp_init = (initproc) _pypolkit_PolkitSystemBusName_init,
    .tp_alloc = PyType_GenericAlloc,
    .tp_new = PyType_GenericNew,
    /* .tp_free = XXX */
    /* .tp_is_gc = XXX */
    .tp_bases = NULL,
    /* .tp_del = XXX */
};

#endif
