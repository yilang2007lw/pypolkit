/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TYPEOBJECTS_POLKIT_AUTHORITY_H
#define TYPEOBJECTS_POLKIT_AUTHORITY_H

#include <Python.h>
#include <structmember.h>

static PyMemberDef _pypolkit_PolkitAuthority_members[] = {
    {"backend_features", T_OBJECT, offsetof(_pypolkit_PolkitAuthority, backend_features), READONLY, "doc"},

    {"backend_name", T_OBJECT, offsetof(_pypolkit_PolkitAuthority, backend_name), READONLY, "doc"},

    {"backend_version", T_OBJECT, offsetof(_pypolkit_PolkitAuthority, backend_version), READONLY, "doc"},

    {"owner", T_OBJECT, offsetof(_pypolkit_PolkitAuthority, owner), READONLY, "doc"},

    {NULL}
};

static PyMethodDef _pypolkit_PolkitAuthority_methods[] = {
    {"check_authorization", (PyCFunction) pypolkit_polkit_authority_check_authorization, METH_VARARGS, "doc" },

    {"enumerate_actions", (PyCFunction) pypolkit_polkit_authority_enumerate_actions, METH_VARARGS, "doc" },

    {"register_authentication_agent", (PyCFunction) pypolkit_polkit_authority_register_authentication_agent, METH_VARARGS, "doc" },

    {"register_authentication_agent_with_options", (PyCFunction) pypolkit_polkit_authority_register_authentication_agent_with_options, METH_VARARGS, "doc" },

    {"unregister_authentication_agent", (PyCFunction) pypolkit_polkit_authority_unregister_authentication_agent, METH_VARARGS, "doc" },

    {"authentication_agent_response", (PyCFunction) pypolkit_polkit_authority_authentication_agent_response, METH_VARARGS, "doc" },

    {"enumerate_temporary_authorizations", (PyCFunction) pypolkit_polkit_authority_enumerate_temporary_authorizations, METH_VARARGS, "doc" },

    {"revoke_temporary_authorizations_by_id", (PyCFunction) pypolkit_polkit_authority_revoke_temporary_authorizations_by_id, METH_VARARGS, "doc" },

    {NULL}
};

static PyGetSetDef _pypolkit_PolkitAuthority_getset[] = {
    {"backend_name", (getter) _pypolkit_PolkitAuthority_get, NULL, "doc"},

    {"backend_version", (getter) _pypolkit_PolkitAuthority_get, NULL, "doc"},

    {"backend_features", (getter) _pypolkit_PolkitAuthority_get, NULL, "doc"},

    {"owner", (getter) _pypolkit_PolkitAuthority_get, NULL, "doc"},

    {NULL}
};


PyTypeObject _pypolkit_PolkitAuthority_Type_obj = {
    PyObject_HEAD_INIT(&PyType_Type)
    .tp_name = "_pypolkit.PolkitAuthority",
    .tp_basicsize = sizeof(_pypolkit_PolkitAuthority),
    /* .tp_itemsize = XXX */
    .tp_dealloc = (destructor) _pypolkit_PolkitAuthority_dealloc,
    /* .tp_getattr = XXX */
    /* .tp_setattr = XXX */
    /* .tp_compare = (cmpfunc) _pypolkit_PolkitAuthority_compare, */
    /* .tp_repr = XXX */
    /* .tp_as_number = XXX */
    /* .tp_as_sequence = XXX */
    /* .tp_as_mapping = XXX */
    .tp_hash = PyObject_HashNotImplemented,
    .tp_call = NULL,
    /* .tp_str = (reprfunc) _pypolkit_PolkitAuthority_str, */
    .tp_getattro = PyObject_GenericGetAttr,
    .tp_setattro = PyObject_GenericSetAttr,
    /* .tp_as_buffer = XXX */
    .tp_flags = Py_TPFLAGS_HAVE_CLASS | Py_TPFLAGS_CHECKTYPES |
    Py_TPFLAGS_HAVE_GC | Py_TPFLAGS_BASETYPE,
    .tp_doc = "polkit PolkitAuthority doc",
    .tp_traverse = (traverseproc) _pypolkit_PolkitAuthority_traverse,
    .tp_clear = (inquiry) _pypolkit_PolkitAuthority_clear,
    /* .tp_richcompare = (richcmpfunc) _pypolkit_PolkitAuthority_richcompare, */
    /* .tp_weaklistoffset = XXX */
    /* .tp_iter = XXX */
    /* .tp_iternext = XXX */
    .tp_methods = _pypolkit_PolkitAuthority_methods,
    .tp_members = _pypolkit_PolkitAuthority_members,
    .tp_getset = _pypolkit_PolkitAuthority_getset,
    .tp_base = NULL,
    .tp_dict = NULL,
    /* .tp_descr_get = XXX */
    /* .tp_descr_set = XXX */
    /* .tp_dictoffset = XXX */
    .tp_init = (initproc) _pypolkit_PolkitAuthority_init,
    .tp_alloc = PyType_GenericAlloc,
    .tp_new = PyType_GenericNew,
    /* .tp_free = XXX */
    /* .tp_is_gc = XXX */
    .tp_bases = NULL,
    /* .tp_del = XXX */
};

#endif
