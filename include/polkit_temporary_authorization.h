 /* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_TEMPORARY_AUTHORIZATION_H
#define PYPOLKIT_POLKIT_TEMPORARY_AUTHORIZATION_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD

}_pypolkit_PolkitTemporaryAuthorization;

void _pypolkit_PolkitTemporaryAuthorization_dealloc(_pypolkit_PolkitTemporaryAuthorization *);
int _pypolkit_PolkitTemporaryAuthorization_traverse(_pypolkit_PolkitTemporaryAuthorization *, visitproc, void *);
int _pypolkit_PolkitTemporaryAuthorization_clear(_pypolkit_PolkitTemporaryAuthorization *);
int _pypolkit_PolkitTemporaryAuthorization_init(_pypolkit_PolkitTemporaryAuthorization *, PyObject *, PyObject *);

PyObject * pypolkit_polkit_temporary_authorization_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_temporary_authorization_get_id(PyObject *, PyObject *);
PyObject * pypolkit_polkit_temporary_authorization_get_action_id(PyObject *, PyObject *);
PyObject * pypolkit_polkit_temporary_authorization_get_subject(PyObject *, PyObject *);
PyObject * pypolkit_polkit_temporary_authorization_get_time_obtained(PyObject *, PyObject *);
PyObject * pypolkit_polkit_temporary_authorization_get_time_expires(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitTemporaryAuthorization_Type_obj;

#endif
