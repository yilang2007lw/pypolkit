/* 
 * Copyright (C) 2012 Deepin, Inc.
 *               2012 Long Wei
 *
 * Author:     Long Wei <yilang2007lw at gmail.com>
 * Maintainer: Long Wei <yilang2007lw at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PYPOLKIT_POLKIT_ACTION_DESCRIPTION_H
#define PYPOLKIT_POLKIT_ACTION_DESCRIPTION_H

#include <Python.h>

#include <polkit/polkit.h>

typedef struct{
    PyObject_HEAD

        }_pypolkit_PolkitActionDescription;

void _pypolkit_PolkitActionDescription_dealloc(_pypolkit_PolkitActionDescription *);
int _pypolkit_PolkitActionDescription_traverse(_pypolkit_PolkitActionDescription *, visitproc, void *);
int _pypolkit_PolkitActionDescription_clear(_pypolkit_PolkitActionDescription *);
int _pypolkit_PolkitActionDescription_init(_pypolkit_PolkitActionDescription *, PyObject *, PyObject *);

PyObject * pypolkit_polkit_action_description_new(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_action_id(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_description(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_message(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_vendor_name(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_vendor_url(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_icon_name(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_implicit_any(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_implicit_inactive(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_implicit_active(PyObject *, PyObject *);
PyObject * pypolkit_polkit_action_description_get_annotation(PyObject *, PyObject *);

extern PyTypeObject _pypolkit_PolkitActionDescription_Type_obj;

#endif
